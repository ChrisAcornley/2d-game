#include "Movement.h"

Movement::Movement(void)
{
	// Nothing
}

Movement::~Movement(void)
{
	// Nothing
}

void Movement::PlayerMovement_MoveX(int nx)
{
	playerCurrentPosX += nx;
}

void Movement::PlayerMovement_MoveY(int ny)
{
	playerCurrentPosY += ny;
}

int Movement::PlayerMovement_GetX()
{
	return playerCurrentPosX;
}

int Movement::PlayerMovement_GetY()
{
	return playerCurrentPosX;
}

void Movement::PlayerMovement_StartPosition(int sx, int sy)
{
	playerCurrentPosX = sx;
	playerCurrentPosY = sy;
}