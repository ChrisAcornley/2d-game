#include "Player.h"

Player::Player(void)
{
	SetX(240);
	SetY(240);
	SetOffsetX(0);
	SetOffsetY(0);
	playerCurrentPosX = 7;
	playerCurrentPosY = 7;
	hasDivingHelmet = false;
	hasFireHat = false;
	dead = false;
}

Player::~Player(void)
{
	// Nothing to clean
}

int Player::GetPlayerX()
{
	return playerCurrentPosX;
}

int Player::GetPlayerY()
{
	return playerCurrentPosY;
}

void Player::MovePlayerX(int nx)
{
	playerCurrentPosX += nx;
}

void Player::MovePlayerY(int ny)
{
	playerCurrentPosY += ny;
}

void Player::SetPlayerCoord(int nx, int ny)
{
	playerCurrentPosX = nx;
	playerCurrentPosY = ny;
}

void Player::CurrentMapBlockCheck(int setOffset, int blockID)
{
	offsety = setOffset;

	switch(blockID)
	{
	case 0:
		offsetx = 0;
		break;

	case 2:
		if(hasFireHat)
		{
			offsetx = 48;
		}
		else
		{
			offsetx = 96;
			dead = true;
		}
		break;

	case 3:
		if(hasDivingHelmet)
		{
			offsetx = 144;
		}
		else
		{
			offsetx = 192;
			dead = true;
		}
		break;

	case 4:
		nextLevel = true;
		break;

	default:
		break;
	}
}

/*
Author:			Christopher Acornley (1000697)
Date:			05/01/12
Name:			CurrentObjectBlockCheck
Description:	Checks the space beneath the player to see if there is an object that can be picked up or moved
Notes:			N/A
*/
void Player::CurrentObjectBlockCheck(int setOffset, int blockID)
{
	offsety = setOffset;

	switch(blockID)
	{
	case 0:
		break;

	case 1:
		break;

	case 2:
		hasFireHat = true;
		break;

	case 3:
		hasDivingHelmet = true;
		break;

	default:
		break;
	}
}

/*
Author:			Christopher Acornley (1000697)
Date:			31/11/12
Name:			PlayerDead
Description:	Checks if the player is deads
Notes:			N/A
*/
bool Player::PlayerDead()
{
	return dead;
}

/*
Author:			Christopher Acornley (1000697)
Date:			05/01/12
Name:			ProceedNextLevel
Description:	Checks if the game should go to the next level
Notes:			N/A
*/
bool Player::ProceedNextLevel()
{
	return nextLevel;
}

/*
Author:			Christopher Acornley (1000697)
Date:			05/01/12
Name:			PlayerDead
Description:	Sets the level check to false to keep it on the current level
Notes:			N/A
*/
void Player::StayOnLevel()
{
	nextLevel = false;
}

/*
Author:			Christopher Acornley (1000697)
Date:			05/01/12
Name:			PlayerHasFireHat
Description:	Checks status of fire hat
Notes:			N/A
*/
bool Player::PlayerHasFireHat()
{
	return hasFireHat;
}

/*
Author:			Christopher Acornley (1000697)
Date:			05/01/12
Name:			PlayerHasDivingHelmet
Description:	Checks status of diving helmet
Notes:			N/A
*/
bool Player::PlayerHasDivingHelmet()
{
	return hasDivingHelmet;
}

void Player::PlayerLostInventory()
{
	hasFireHat = false;
	hasDivingHelmet = false;
}

bool Player::ResetLevel()
{
	return resetLevel;
}

void Player::PlayerResetLevel()
{
	if(resetLevel)
		resetLevel = false;

	else if(!resetLevel)
		resetLevel = true;
}

void Player::PlayerAlive()
{
	dead = false;
}
