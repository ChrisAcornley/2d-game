#ifndef _ENGINE_H_
#define _ENGINE_H_

#include <windows.h>

class Engine
{
public:

	Engine();

	~Engine();

	void Engine::releaseResources();

	void Engine::displayFrame();

	void Engine::setBuffers();

	void Engine::drawSprite(int x, int y, int height, int width, HBITMAP Bitmap, int offsetx, int offsety);

	HBITMAP		theOldFrontBitMap, theOldBackBitMap;

	HWND        ghwnd;

	RECT		screenRect;

	HDC			backHDC, frontHDC, bitmapHDC;	//Hardware device contexts for the Buffers

private:

};

#endif