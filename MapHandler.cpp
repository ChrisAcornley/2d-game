#include "MapHandler.h"

MapHandler::MapHandler()
{
	level = 0;
	mapGold = 0;
	PassMap();
	PassObjectMap();

}

MapHandler::~MapHandler()
{

}

int MapHandler::GetLevelNumber()
{
	return level;
}

void MapHandler::SetLevel(int nl)
{
	level = nl;
}

void MapHandler::NextLevel()
{
	level++;
}

void MapHandler::ObjectMover(int px, int py)
{
	if(currentObjectMap[py][px] > 1)
	{
		currentObjectMap[py][px] = 0;
	}

	else if(currentObjectMap[py][px] == 1)
	{
		currentObjectMap[py][px] = 0;
		mapGold --;
	}

	else
	{
	}
}

int MapHandler::GoldOnMap()
{
	return mapGold;
}

int MapHandler::GetexitBlockerX()
{
	return exitBlockerX;
}

int MapHandler::GetexitBlockerY()
{
	return exitBlockerY;
}

void MapHandler::ResetGoldNumber()
{
	mapGold = 0;
}

