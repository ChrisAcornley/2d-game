#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Sprite.h"

class Player : public Sprite
{
public:

	Player(void);

	~Player(void);

	int GetPlayerX();

	int GetPlayerY();

	void MovePlayerX(int nx);

	void MovePlayerY(int ny);

	void SetPlayerCoord(int nx, int ny);

	void CurrentMapBlockCheck(int setOffset, int blockID);

	void CurrentObjectBlockCheck(int setOffset, int blockID);

	bool PlayerDead();

	bool ProceedNextLevel();

	void StayOnLevel();

	bool PlayerHasFireHat();

	bool PlayerHasDivingHelmet();

	void PlayerLostInventory();

	bool ResetLevel();

	void PlayerResetLevel();

	void PlayerAlive();

private:

	int playerCurrentPosX, playerCurrentPosY;		// Players position relative to map
	
	int playerOffsetX, playerOffsetY;				// Players specific offset

	bool hasDivingHelmet, hasFireHat;				// Checks if the player can go onto fire and water blocks

	bool dead;										// Checks if the player has died

	bool nextLevel;									// Prompts the game to go to the next level

	bool resetLevel;								// Promts the game to reset the level

};

#endif