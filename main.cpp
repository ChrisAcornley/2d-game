//Windows Example Code
//Matthew Bett 2005
// main.cpp is being used as the engine so all the draw sprite functions, setting buffers etc is in this file

#include <windows.h>
#include <stdio.h>
#include <mmsystem.h>
#include <math.h>

#include "Player.h"
#include "Floor.h"
#include "StoneBlock.h"
#include "ExitBlock.h"
#include "WaterBlock.h"
#include "FireBlock.h"
#include "MapHandler.h"
#include "FireHat.h"
#include "ExitBlocker.h"
#include "BackGround.h"
#include "SideDisplay.h"
#include "DivingHelmet.h"
#include "GoldCoin.h"
#include "CoinNumber.h"

typedef struct Mouse
{
	int x,y;
}Mouse;

int ticker =0;

HBITMAP		theOldFrontBitMap, theOldBackBitMap;
HWND        ghwnd;
RECT		screenRect;
HDC			backHDC, frontHDC, bitmapHDC;	//Hardware device contexts for the Buffers

bool		keys[256];

Mouse MousePos;
Floor floorblock;
StoneBlock stoneblock;
Player player;
ExitBlock exitblock;
WaterBlock waterblock;
FireBlock fireblock;
MapHandler MHand;
FireHat firehat;
ExitBlocker exitblocker;
BackGround background;
SideDisplay sidedisplay;
DivingHelmet divinghelmet;
GoldCoin goldcoin;
CoinNumber coinnumber[10];

bool PlayerCollision(int direction);
void DrawPlayer();
void Draw(Sprite r1, int mapx, int mapy);
bool SetBitmaps();
void DeathMessage(bool &x);
void DrawSolidMap(int ix, int jy, int sx, int sy);
void DrawObjectMap(int ix, int jy, int sx, int sy);
void DrawSideDisplay(bool fh, bool dh);
void DrawBackground();
void ResetLevel();
void DrawRemainingCoins();
BOOL waitFor(unsigned long delay);
LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);

void RegisterMyWindow(HINSTANCE hInstance)
{
    WNDCLASSEX  wcex;									

    wcex.cbSize        = sizeof (wcex);				
    wcex.style         = CS_HREDRAW | CS_VREDRAW;		
    wcex.lpfnWndProc   = WndProc;						
    wcex.cbClsExtra    = 0;								
    wcex.cbWndExtra    = 0;								
    wcex.hInstance     = hInstance;						
    wcex.hIcon         = 0; 
    wcex.hCursor       = LoadCursor (NULL, IDC_ARROW);	
															
    wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW+1);
    wcex.lpszMenuName  = NULL;							
    wcex.lpszClassName = "FirstWindowClass";				
    wcex.hIconSm       = 0; 

	RegisterClassEx (&wcex);							
}


BOOL InitialiseMyWindow(HINSTANCE hInstance, int nCmdShow)
{
	HWND        hwnd;
	hwnd = CreateWindow ("FirstWindowClass",					
						 "Gold Rush",		  	
						 WS_OVERLAPPEDWINDOW,	
						 200,			
						 100,			
						 880,			
						 566,			
						 NULL,					
						 NULL,					
						 hInstance,				
						 NULL);								
	if (!hwnd)
	{
		return FALSE;
	}

    ShowWindow (hwnd, nCmdShow);						
    UpdateWindow (hwnd);	
	ghwnd = hwnd;
	return TRUE;

}



BOOL WaitFor(unsigned long delay)
{
	static unsigned long clockStart = 0;
	unsigned long timePassed;
	unsigned long now = timeGetTime();

	timePassed = now - clockStart;
	if (timePassed >  delay)
	{
		clockStart = now;
		return TRUE;
	}
	else
		return FALSE;
}
	
			
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)											
    {														
		case WM_CREATE:	
			break;

		case WM_SIZE:
			break;	

		case WM_KEYDOWN:
			keys[wParam]=true;
			break;

		case WM_KEYUP:
			keys[wParam]=false;
			break;

		case WM_MOUSEMOVE:
			MousePos.x = LOWORD (lParam);
			MousePos.y = HIWORD (lParam);
			break;

		case WM_PAINT:
			
	
		    break;		

		case WM_DESTROY:	
			
			PostQuitMessage(0);	
								
			break;				
	}													

	return DefWindowProc (hwnd, message, wParam, lParam);		
															
}

HBITMAP LoadABitmap(LPSTR szFileName)
{
	return (HBITMAP)LoadImage(NULL, szFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
}

void drawSprite(int x, int y, int height, int width, HBITMAP Bitmap, int offsetx, int offsety)
{
	HBITMAP originalBitMap;
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC,Bitmap);
	GdiTransparentBlt(backHDC,x,y,height,width,bitmapHDC,offsetx,offsety,height,width,0xFF00FF);
	SelectObject(bitmapHDC,originalBitMap); 
}

void setBuffers()
{
	GetClientRect(ghwnd, &screenRect);	//creates rect based on window client area
	frontHDC = GetDC(ghwnd);			// Initialises front buffer device context (window)
	backHDC = CreateCompatibleDC(frontHDC);// sets up Back DC to be compatible with the front
	bitmapHDC=CreateCompatibleDC(backHDC);
	theOldFrontBitMap = CreateCompatibleBitmap(frontHDC, screenRect.right, 
		screenRect.bottom);		//creates bitmap compatible with the front buffer
    theOldBackBitMap = (HBITMAP)SelectObject(backHDC, theOldFrontBitMap);
								//creates bitmap compatible with the back buffer
	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));	
}

void displayFrame()
{
		BitBlt(frontHDC, screenRect.left,screenRect.top, screenRect.right, 
		screenRect.bottom, backHDC, 0, 0, SRCCOPY);
		FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));	
}

void releaseResources()
{
	SelectObject(backHDC,theOldBackBitMap);
	DeleteDC(backHDC);
	DeleteDC(bitmapHDC);
	ReleaseDC(ghwnd,frontHDC);
}


int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int nCmdShow)			
{									
    MSG         msg;	
	HDC	hdcWindow;

	int screenx = 48;
	int screeny = 48;
	int drawingStartX = 3;
	int drawingStartY = 3;
	bool gameLoop = true;

	SetBitmaps();

	RegisterMyWindow(hInstance);

   	if (!InitialiseMyWindow(hInstance, nCmdShow))
	return FALSE;
	
	setBuffers();

	while (gameLoop)					
    {							
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
		    if (msg.message==WM_QUIT)
				break;
			TranslateMessage (&msg);							
			DispatchMessage (&msg);
		}

		else
		{		
			if(player.ProceedNextLevel())
			{
				MHand.NextLevel();
				player.PlayerLostInventory();
				MHand.PassMap();
				MHand.PassObjectMap();
				drawingStartX = 3;
				drawingStartY = 3;
				player.SetPlayerCoord(7,7);
				player.StayOnLevel();
			}

			if(player.ResetLevel())
			{
				MHand.currentMap[MHand.GetexitBlockerX()][MHand.GetexitBlockerY()] = 5;
				player.PlayerLostInventory();
				player.PlayerAlive();
				MHand.ResetGoldNumber();
				MHand.PassObjectMap();
				player.SetOffsetX(0);
				player.SetOffsetY(0);
				drawingStartX = 3;
				drawingStartY = 3;
				player.SetPlayerCoord(7,7);
				player.PlayerResetLevel();
			}

			if(keys['A'])
			{
				if(!PlayerCollision(1))
				{
					drawingStartX --;
					player.MovePlayerX(-1);
				}
				player.CurrentMapBlockCheck(96, MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()]);
				player.CurrentObjectBlockCheck(96, MHand.currentObjectMap[player.GetPlayerY()][player.GetPlayerX()]);
				MHand.ObjectMover(player.GetPlayerX(), player.GetPlayerY());
				keys['A']=false;
			}

			if(keys['D'])
			{
				if(!PlayerCollision(2))
				{
					drawingStartX ++;
					player.MovePlayerX(1);
				}
				player.CurrentMapBlockCheck(144, MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()]);
				player.CurrentObjectBlockCheck(144, MHand.currentObjectMap[player.GetPlayerY()][player.GetPlayerX()]);
				MHand.ObjectMover(player.GetPlayerX(), player.GetPlayerY());
				keys['D']=false;
			}

			if(keys['W'])
			{
				if(!PlayerCollision(3))
				{
					drawingStartY --;
					player.MovePlayerY(-1);
				}
				player.CurrentMapBlockCheck(0, MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()]);
				player.CurrentObjectBlockCheck(0, MHand.currentObjectMap[player.GetPlayerY()][player.GetPlayerX()]);
				MHand.ObjectMover(player.GetPlayerX(), player.GetPlayerY());
				keys['W']=false;
			}

			if(keys['S'])
			{
				if(!PlayerCollision(4))
				{
					drawingStartY ++;
					player.MovePlayerY(1);
				}
				player.CurrentMapBlockCheck(48, MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()]);
				player.CurrentObjectBlockCheck(48, MHand.currentObjectMap[player.GetPlayerY()][player.GetPlayerX()]);
				MHand.ObjectMover(player.GetPlayerX(), player.GetPlayerY());
				keys['S']=false;
			}

			if (WaitFor(10))
			{	

				DrawBackground();

				for(int i = drawingStartY; i<(drawingStartY + 9); i++)
				{
					for(int j = drawingStartX; j<(drawingStartX + 9); j++)
					{
						DrawSolidMap(i,j,screenx,screeny);
						DrawObjectMap(i,j,screenx,screeny);
						screenx += 48;
					}
					screeny += 48;
					screenx = 48;
				}
				screenx = 48;
				screeny = 48;
						
				DrawPlayer();	

				DrawSideDisplay(player.PlayerHasFireHat(), player.PlayerHasDivingHelmet());

				DrawRemainingCoins();
					
				displayFrame();	
				
				if(player.PlayerDead())
				{
					DeathMessage(gameLoop);
				}

				if(MHand.GoldOnMap() == 0)
				{
					MHand.currentMap[MHand.GetexitBlockerX()][MHand.GetexitBlockerY()] = 0;
				}
			}
		}
    }
    releaseResources();
	return msg.wParam ;										
}

bool SetBitmaps()
{
	player.SetBitmap(LoadABitmap("icon.bmp"));
	if (!player.GetBitmap())
		return FALSE;

	stoneblock.SetBitmap(LoadABitmap("StoneBlock.bmp"));
	if(!stoneblock.GetBitmap())
		return FALSE;

	floorblock.SetBitmap(LoadABitmap("Floor.bmp"));
	if(!floorblock.GetBitmap())
		return FALSE;

	exitblock.SetBitmap(LoadABitmap("ExitBlock.bmp"));
	if(!exitblock.GetBitmap())
		return FALSE;

	waterblock.SetBitmap(LoadABitmap("WaterBlock.bmp"));
	if(!waterblock.GetBitmap())
		return FALSE;

	fireblock.SetBitmap(LoadABitmap("FireBlock.bmp"));
	if(!fireblock.GetBitmap())
		return FALSE;

	firehat.SetBitmap(LoadABitmap("FireHat.bmp"));
	if(!firehat.GetBitmap())
		return FALSE;

	exitblocker.SetBitmap(LoadABitmap("ExitBlocker.bmp"));
	if(!exitblocker.GetBitmap())
		return FALSE;

	background.SetBitmap(LoadABitmap("Background.bmp"));
	if(!background.GetBitmap())
		return FALSE;

	sidedisplay.SetBitmap(LoadABitmap("SideDisplay.bmp"));
	if(!sidedisplay.GetBitmap())
		return FALSE;

	divinghelmet.SetBitmap(LoadABitmap("DivingHelmet.bmp"));
	if(!divinghelmet.GetBitmap())
		return FALSE;

	goldcoin.SetBitmap(LoadABitmap("GoldCoin.bmp"));
	if(!goldcoin.GetBitmap())
		return FALSE;

	coinnumber[0].SetBitmap(LoadABitmap("Zero.bmp"));
	if(!coinnumber[0].GetBitmap())
		return FALSE;

	coinnumber[1].SetBitmap(LoadABitmap("One.bmp"));
	if(!coinnumber[1].GetBitmap())
		return FALSE;

	coinnumber[2].SetBitmap(LoadABitmap("Two.bmp"));
	if(!coinnumber[2].GetBitmap())
		return FALSE;

	coinnumber[3].SetBitmap(LoadABitmap("Three.bmp"));
	if(!coinnumber[3].GetBitmap())
		return FALSE;

	coinnumber[4].SetBitmap(LoadABitmap("Four.bmp"));
	if(!coinnumber[4].GetBitmap())
		return FALSE;

	coinnumber[5].SetBitmap(LoadABitmap("Five.bmp"));
	if(!coinnumber[5].GetBitmap())
		return FALSE;

	coinnumber[6].SetBitmap(LoadABitmap("Six.bmp"));
	if(!coinnumber[6].GetBitmap())
		return FALSE;

	coinnumber[7].SetBitmap(LoadABitmap("Seven.bmp"));
	if(!coinnumber[7].GetBitmap())
		return FALSE;

	coinnumber[8].SetBitmap(LoadABitmap("Eight.bmp"));
	if(!coinnumber[8].GetBitmap())
		return FALSE;

	coinnumber[9].SetBitmap(LoadABitmap("Nine.bmp"));
	if(!coinnumber[9].GetBitmap())
		return FALSE;

	return true;
}

/*
Author:			Christopher Acornley (1000697)
Date:			16/11/11
Name:			DrawPlayer
Description:	Draws the players sprite
Notes:			N/A
*/
void DrawPlayer(){

	drawSprite(	player.GetX(), 
				player.GetY(), 
				player.GetHeight(), 
				player.GetWidth(), 
				player.GetBitmap(), 
				player.GetOffsetX(), 
				player.GetOffsetY());
}

bool PlayerCollision(int direction)
{
	switch(direction)
	{
	case 1:
		if(MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()-1] > 5)
			return true;

		else if(MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()-1] == 5 && MHand.GoldOnMap() != 0)
			return true;

		else
			return false;

		break;

	case 2:
		if(MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()+1] > 5)
			return true;
		
		if(MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()+1] == 5 && MHand.GoldOnMap() != 0)
			return true;

		else
			return false;

		break;

	case 3:
		if(MHand.currentMap[player.GetPlayerY()-1][player.GetPlayerX()] > 5)
			return true;

		if(MHand.currentMap[player.GetPlayerY()-1][player.GetPlayerX()] == 5 && MHand.GoldOnMap() != 0)
			return true;

		else
			return false;

		break;

	case 4:
		if(MHand.currentMap[player.GetPlayerY()+1][player.GetPlayerX()] > 5)
			return true;

		if(MHand.currentMap[player.GetPlayerY()+1][player.GetPlayerX()] == 5 && MHand.GoldOnMap() != 0)
			return true;
		
		else
			return false;

		break;

	default:
		return false;

		break;
	}
}


void Draw(Sprite r1, int mapx, int mapy)
{
	drawSprite(	mapx, 
				mapy, 
				r1.GetHeight(), 
				r1.GetWidth(), 
				r1.GetBitmap(), 
				r1.GetOffsetX(), 
				r1.GetOffsetY());
}

void DeathMessage(bool &x)
{
	switch(MHand.currentMap[player.GetPlayerY()][player.GetPlayerX()])
	{
	case 2:
		MessageBox(ghwnd, "You can't walk on fire without a Fire Hat.", "Bummer!", MB_OK);
		player.PlayerResetLevel();
		break;
	case 3:
		MessageBox(ghwnd, "You can't walk on water without a Diving Helmet.", "Bummer!", MB_OK);
		player.PlayerResetLevel();
		break;
	default:
		break;
	}
}

void DrawSolidMap(int ix,int jy,int sx,int sy)
{
	switch (MHand.currentMap[ix][jy])
	{
	case 0:
		Draw(floorblock, sx, sy);
		break;
	case 2:
		Draw(fireblock, sx, sy);
		break;
	case 3:
		Draw(waterblock, sx, sy);
		break;
	case 4:
		Draw(exitblock, sx, sy);
		break;
	case 5:
		Draw(exitblocker, sx, sy);
		break;
	case 6:
		Draw(stoneblock, sx, sy);
		break;
	default:
		break;
	}
}

void DrawObjectMap(int ix,int jy,int sx,int sy)
{
	switch (MHand.currentObjectMap[ix][jy])
	{
	case 0:
		break;
	case 1:
		Draw(goldcoin, sx, sy);
		break;
	case 2:
		Draw(firehat, sx, sy);
		break;
	case 3:
		Draw(divinghelmet, sx, sy);
		break;
	default:
		break;
	}
}

/*
Author:			Christopher Acornley (1000697)
Date:			05/01/12
Name:			DrawSideDisplay
Description:	Draws the side Display
Notes:			N/A
*/
void DrawSideDisplay(bool fh, bool dh)
{
	Draw(sidedisplay, 576, 48);
	
	if(fh)
		Draw(firehat, 623, 353);

	if(dh)
		Draw(divinghelmet, 671, 353);
}

/*
Author:			Christopher Acornley (1000697)
Date:			05/01/12
Name:			DrawBackground
Description:	Draws the background
Notes:			N/A
*/
void DrawBackground()
{
	Draw(background,0,0);
}

void DrawRemainingCoins()
{
	int number[3];
	int sx = 607;
	number[0] = MHand.GoldOnMap() / 100;
	number[1] = (MHand.GoldOnMap() / 10) - number[0]*10;
	number[2] = MHand.GoldOnMap() - (number[0]*100) - (number[1]*10);

	for(int i = 0; i < 3; i++)
	{
		switch(number[i])
		{
		case 0:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 1:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 2:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 3:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 4:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 5:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 6:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 7:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 8:
			Draw(coinnumber[number[i]],sx,218);
			break;
		case 9:
			Draw(coinnumber[number[i]],sx,218);
			break;

		default:
			break;
		}
		sx += 42;
	}
}