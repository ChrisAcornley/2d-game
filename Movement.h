#ifndef _MOVEMENT_H_
#define _MOVEMENT_H_

class Movement
{
public:

	Movement(void);

	~Movement(void);

	void PlayerMovement_MoveX(int nx);

	void PlayerMovement_MoveY(int ny);

	int PlayerMovement_GetX();

	int PlayerMovement_GetY();

	void PlayerMovement_StartPosition(int sx, int sy);

private:

	int playerCurrentPosX, playerCurrentPosY;

protected:
};

#endif