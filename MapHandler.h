#ifndef _MAPHANDLER_H_
#define _MAPHANDLER_H_

class MapHandler
{
	public:

		MapHandler();

		~MapHandler();

		int GetLevelNumber();

		void SetLevel(int nl);

		void NextLevel();

		int currentMap[15][15];			// Handles the current map;

		int currentObjectMap[15][15];	// Handles the objects on the map

		void ObjectMover(int px, int py);	// Moves or removes objects

		void PassMap();					// Passes the map to be used

		void PassObjectMap();			// Passes the object map to be used

		int GoldOnMap();				// Returns the number of gold pieces on the map

		void ResetGoldNumber();			// Resets the gold number

		int GetexitBlockerX();			// Returns X co-ord

		int GetexitBlockerY();			// Returns Y Co-ord


	private:

		int level;					// Handles which level the player is on

		int mapGold;				// Handles the number of gold pieces on the map

		int exitBlockerX;			// X Co-ords of the exit blocker

		int exitBlockerY;			// Y Co-ords of the exit blocker
};

#endif