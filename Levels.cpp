#include "MapHandler.h"

int mapLevel[2][15][15] = {
						   {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,6,6,6,6,6,6,6,6,6,0,0,0},
							{0,0,0,6,0,6,0,6,0,5,4,6,0,0,0},
							{0,0,0,6,3,6,0,6,0,6,6,6,0,0,0},
							{0,0,0,6,3,0,0,0,0,6,0,6,0,0,0},
							{0,0,0,6,0,6,6,0,0,6,0,6,0,0,0},
							{0,0,0,6,6,0,6,6,0,6,0,6,0,0,0},
							{0,0,0,6,2,2,0,6,0,6,0,6,0,0,0},
							{0,0,0,6,0,6,0,0,0,0,0,6,0,0,0},
							{0,0,0,6,6,6,6,6,6,6,6,6,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}},

						   {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,6,6,6,6,6,6,6,6,6,0,0,0},
							{0,0,0,6,0,0,0,0,0,0,0,6,0,0,0},
							{0,0,0,6,0,2,0,0,6,6,0,6,0,0,0},
							{0,0,0,6,3,6,3,0,0,0,0,6,0,0,0},
							{0,0,0,6,0,6,0,0,0,0,0,6,0,0,0},
							{0,0,0,6,0,6,6,6,3,0,0,6,0,0,0},
							{0,0,0,6,6,0,0,0,0,2,0,6,0,0,0},
							{0,0,0,6,4,5,3,0,0,0,0,6,0,0,0},
							{0,0,0,6,6,6,6,6,6,6,6,6,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}},

						  };
	
int objectLevel[2][15][15] =  {

							   {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,2,0,1,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,3,0,0,0,0},
								{0,0,0,0,1,0,0,0,0,0,1,0,0,0,0},
								{0,0,0,0,0,1,0,0,0,0,1,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
								{0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}},

							   {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,1,1,1,1,1,1,1,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,1,1,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,1,1,0,0,0,0},
								{0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,3,0,1,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,1,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
								{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}},
							
							  };

void MapHandler::PassMap()
{
	for(int i = 0; i < 15; i++)
	{
		for(int j = 0; j < 15; j++)
		{
			currentMap[i][j] = mapLevel[level][i][j];
			if(currentMap[i][j] == 5)
			{
				exitBlockerX = i;
				exitBlockerY = j;
			}
		}
	}
}

void MapHandler::PassObjectMap()
{
	for(int i = 0; i < 15; i++)
	{
		for(int j = 0; j < 15; j++)
		{
			currentObjectMap[i][j] = objectLevel[level][i][j];
			if(objectLevel[level][i][j] == 1)
			{
				mapGold ++;
			}
		}
	}
}
