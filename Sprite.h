#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <windows.h>

class Sprite
{
public:

	Sprite();

	~Sprite();

	int GetX();

	int GetY();

	void SetX(int n_x);

	void SetY(int n_y);

	int GetHeight();

	int GetWidth();

	void SetHeight(int n_h);

	void SetWidth(int n_w);

	int GetOffsetX();

	int GetOffsetY();

	void SetOffsetX(int n_ox);

	void SetOffsetY(int n_oy);

	HBITMAP GetBitmap();

	void SetBitmap(HBITMAP nBitmap);

protected:

	int x, y;
	int height, width;
	int offsetx, offsety;
	HBITMAP bitmap;
};

#endif