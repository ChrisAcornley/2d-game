#include "Sprite.h"

Sprite::Sprite(void)
{
	height = 48;
	width = 48;
}

Sprite::~Sprite(void)
{
	//Nothing to clean
}

int Sprite::GetX()
{
	return x;
}

int Sprite::GetY()
{
	return y;
}

void Sprite::SetX(int n_x)
{
	x = n_x;
}

void Sprite::SetY(int n_y)
{
	y = n_y;
}

int Sprite::GetHeight()
{
	return height;
}

int Sprite::GetWidth()
{
	return width;
}

void Sprite::SetHeight(int n_h)
{
	height = n_h;
}

void Sprite::SetWidth(int n_w)
{
	width = n_w;
}

int Sprite::GetOffsetX()
{
	return offsetx;
}

int Sprite::GetOffsetY()
{
	return offsety;
}

void Sprite::SetOffsetX(int n_ox)
{
	offsetx = n_ox;
}

void Sprite::SetOffsetY(int n_oy)
{
	offsety = n_oy;
}

HBITMAP Sprite::GetBitmap()
{
	return bitmap;
}

void Sprite::SetBitmap(HBITMAP nBitmap)
{
	bitmap = nBitmap;
}

